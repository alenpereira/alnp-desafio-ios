//
//  RepoTableViewController.swift
//  desafio-ios
//
//  Created by Alessandra Pereira on 28/12/2017.
//  Copyright © 2017 Alessandra Pereira. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class RepoTableViewController: UITableViewController {
    @IBOutlet var reposTableView: UITableView!
    
    var allRepos: [DataRepo] = []
    var allOwners: [DataOwner] = []
    
    override func viewWillAppear(_ animated: Bool) {
        
        APIRequest().requestData(page: RepoWrapper.data.page, ViewController: self) {
            self.reposTableView.reloadData()
        }

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        let rows = RepoWrapper.data.repos.count
        if rows > 0 {
            return rows
        } else {
            return 0
        }
    }
    
//    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 120
//    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell( withIdentifier: "repoCell") as! RepoTableViewCell
        let rows = RepoWrapper.data.repos.count
        if rows > 0 {
            cell.repoName.text = RepoWrapper.data.repos[indexPath.row].repoName
            cell.repoDescr.text = RepoWrapper.data.repos[indexPath.row].repoDesc
            cell.branchs.text = String(describing: RepoWrapper.data.repos[indexPath.row].forksCount!)
            cell.favorites.text = String(describing: RepoWrapper.data.repos[indexPath.row].stargazersCount!)
            cell.userName.text = RepoWrapper.data.owners[indexPath.row].ownerName
            cell.userUsername.text = RepoWrapper.data.owners[indexPath.row].ownerUsername
            let url = URL(string: RepoWrapper.data.owners[indexPath.row].ownerAvatar!)
            if let data = try? Data(contentsOf: url!){
                cell.userAvatar.contentMode = .scaleAspectFit
                cell.userAvatar.image = UIImage(data: data)
            }
            
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = (RepoWrapper.data.repos.count) - 1
        if indexPath.row == lastElement {
            APIRequest().requestData(page: (RepoWrapper.data.page + 1), ViewController: self, completion: {
                print("carregou mais?")
                self.reposTableView.reloadData()
                RepoWrapper.data.page += 1
            })
            
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        RepoWrapper.data.selectedRepo = RepoWrapper.data.repos[indexPath.row]
        RepoWrapper.data.selectedOwner = RepoWrapper.data.owners[indexPath.row]
        //self.performSegue(withIdentifier: "openInfo", sender: self)
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let destination = storyboard.instantiateViewController(withIdentifier: "InfoRepoView") as! InfoRepoTableViewController
        navigationController?.pushViewController(destination, animated: true)
        
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
