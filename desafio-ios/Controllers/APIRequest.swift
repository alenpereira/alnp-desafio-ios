//
//  APIRequest.swift
//  desafio-ios
//
//  Created by Alessandra Pereira on 09/01/2018.
//  Copyright © 2018 Alessandra Pereira. All rights reserved.
//

import Foundation
import Alamofire

class APIRequest{
    
    let requesting = DispatchGroup()
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    var allRepos: [DataRepo] = []
    var allOwners: [DataOwner] = []
    
    let issuesRepoURL: String = "https://api.github.com/search/issues?q=repo:"
    let issuesOpenURL: String = "+type:issue+state:open"
    let issuesCloseURL: String = "+type:issue+state:closed"
    let URL: String = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page="
    let pullsURL: String = "https://api.github.com/repos/"
    let pullsEndURL: String = "/pulls"
    
    func requestData(page: Int, ViewController: UIViewController, completion: @escaping () -> Void){
        let dataCount = RepoWrapper.data.owners.count
        APIRequest().requestRepo(URL: URL + "\(page)", ViewController: ViewController, completion: {
            for eachRepo in RepoWrapper.data.repos {
                self.requesting.enter()
                self.requestOwner(URL: eachRepo.ownerURL!, ViewController: ViewController, Pulls: false, completion:{
                    self.requesting.leave()
                })
            }
            self.requesting.notify(queue: .main){
                let subview = ViewController.view.subviews
                for view in subview where view is UIActivityIndicatorView{
                    view.removeFromSuperview()
                }
                completion()
            }
        })
    }
    
    func requestRepo(URL: String, ViewController: UIViewController, completion : @escaping ()->()){
        Alamofire.request(URL).responseJSON { response in
            
            //print("Request: \(String(describing: response.request))")   // original url request
            //print("Response: \(String(describing: response.response))") // http url response
            //print("Result: \(response.result)") // response serialization result
            
            ViewController.view.addSubview(self.activityIndicator)
            self.activityIndicator.frame = ViewController.view.bounds
            self.activityIndicator.startAnimating()
            
            if let json = response.result.value as? [String: Any]{
                if let jsonDataArray = try? json["items"] as? [[String: Any]] {
                    for eachData in jsonDataArray! {
                        if let eachStop = DataRepo(JSON: eachData){
                            RepoWrapper.data.repos.append(eachStop)
                            //self.allRepos.append(eachStop)
                            print(eachStop.description)
                        }
                    }
                    completion()
                }
            }
        }
    }
    
    func requestOwner(URL: String, ViewController: UIViewController, Pulls: Bool, completion: @escaping () -> ()){
        Alamofire.request(URL).responseJSON { response in
            if (response.result.isSuccess) {
                if let json = response.result.value as? [String: Any]{
                    let owner = DataOwner(JSON: json)
                    if (Pulls){
                        RepoWrapper.data.usersPulls.append(owner!)
                    } else {
                        RepoWrapper.data.owners.append(owner!)
                    }
                }
            }
            completion()
        }
    }
    
    func requestPulls(RepoFullName: String, ViewController: UIViewController, completion: @escaping () -> ()){
        
        ViewController.view.addSubview(self.activityIndicator)
        self.activityIndicator.frame = ViewController.view.bounds
        self.activityIndicator.startAnimating()
        let someURL = pullsURL + RepoFullName + pullsEndURL
        print(someURL)
        Alamofire.request(someURL).responseJSON { response in
            if (response.result.isSuccess) {
                if let json = response.result.value as? [[String: Any]]{
                    if let jsonDataArray = try? json as? [[String: Any]] {
                        for eachData in jsonDataArray! {
                            if let eachStop = DataPR(JSON: eachData){
                                RepoWrapper.data.pulls.append(eachStop)
                                print(eachStop.description)
                            }
                        }
                        completion()
                    }
                } else {
                    print("NO DATA")
                    let subview = ViewController.view.subviews
                    for view in subview where view is UIActivityIndicatorView{
                        view.removeFromSuperview()
                    }
                    let alert = UIAlertController(title: "No data found!", message: "This repository has no pull requests to show.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    ViewController.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    func requestUsers(ViewController: UIViewController, completion: @escaping () -> ()){
        for eachPull in RepoWrapper.data.pulls {
            self.requesting.enter()
            self.requestOwner(URL: eachPull.userURL!, ViewController: ViewController, Pulls: true, completion:{
                self.requesting.leave()
            })
        }
        self.requesting.notify(queue: .main){
            let subview = ViewController.view.subviews
            for view in subview where view is UIActivityIndicatorView{
                view.removeFromSuperview()
            }
            completion()
        }
    }
    
    func requestClosedIssues(RepoFullName: String, ViewController: UIViewController, completion: @escaping () -> ()){
        let URL = issuesRepoURL + RepoFullName + issuesCloseURL
        Alamofire.request(URL).responseJSON { response in
            if (response.result.isSuccess) {
                if let json = response.result.value as? [String: Any]{
                    RepoWrapper.data.closedIssuesFromSelectedRepo = try? json["total_count"] as! Int
                    completion()
                }
            }
        }
    }
    
}


