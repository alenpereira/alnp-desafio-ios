//
//  InfoRepoTableViewCell.swift
//  desafio-ios
//
//  Created by Alessandra Pereira on 30/12/2017.
//  Copyright © 2017 Alessandra Pereira. All rights reserved.
//

import UIKit

class InfoRepoTableViewCell: UITableViewCell {
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var userCompleteName: UILabel!
    @IBOutlet weak var tittleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
