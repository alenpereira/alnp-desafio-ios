//
//  RepoTableViewCell.swift
//  desafio-ios
//
//  Created by Alessandra Pereira on 28/12/2017.
//  Copyright © 2017 Alessandra Pereira. All rights reserved.
//

import UIKit

class RepoTableViewCell: UITableViewCell {
    @IBOutlet weak var repoName: UILabel!
    @IBOutlet weak var repoDescr: UILabel!
    @IBOutlet weak var branchs: UILabel!
    @IBOutlet weak var favorites: UILabel!
    @IBOutlet weak var userAvatar: UIImageView!
    @IBOutlet weak var userName: UILabel!
    
    @IBOutlet weak var userUsername: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
