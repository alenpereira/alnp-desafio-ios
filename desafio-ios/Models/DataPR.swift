//
//  DataRepo.swift
//  desafio-ios
//
//  Created by Alessandra Pereira on 31/12/2017.
//  Copyright © 2018 Alessandra Pereira. All rights reserved.
//

import Foundation
import ObjectMapper

class DataPR: Mappable{
    
    var prId: Int?
    var prURL: String?
    var prHtmlURL: String?
    var prTittle: String?
    var prDate: String?
    var prBody: String?
    var userId: Int?
    var userURL: String?
    var userUsername: String?
    var userAvatar: String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    private func unwrapedDescription(value: Any?) -> String {
        if let value = value {
            return "\(value)"
        }
        return "[no data]"
    }
    
    var description: String {
        var _result = " issueId: \(unwrapedDescription(value: prId))\n"
        _result += " issueTittle: \(unwrapedDescription(value: prTittle))\n"
        _result += " userId: \(unwrapedDescription(value: userId))\n"
        _result += " userURL: \(unwrapedDescription(value: userURL))\n"
        return _result
    }
    
    func mapping(map: Map) {

        prId                <- map["id"]
        prURL               <- map["url"]
        prHtmlURL           <- map["html_url"]
        prTittle            <- map["title"]
        prDate              <- map["created_at"]
        prBody              <- map["body"]
        userId              <- map["user.id"]
        userURL             <- map["user.url"]
        userUsername        <- map["user.login"]
        userAvatar          <- map["user.avatar_url"]
    }
    
    // MARK: Endpoints
    class func endpointForRepo() -> String {
        return "https://api.github.com/search/repositories?q=language:Java&sort=stars"
    }
    
}

