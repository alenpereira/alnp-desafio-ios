//
//  DataOwner.swift
//  desafio-ios
//
//  Created by Alessandra Pereira on 31/12/2017.
//  Copyright © 2018 Alessandra Pereira. All rights reserved.
//

import Foundation
import ObjectMapper

class DataOwner: Mappable{
    
    var ownerId: Int?
    var ownerURL: String?
    var ownerUsername: String?
    var ownerName: String?
    var ownerAvatar: String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    private func unwrapedDescription(value: Any?) -> String {
        if let value = value {
            return "\(value)"
        }
        return "[no data]"
    }
    
    var description: String {
        var _result = " ownerId: \(unwrapedDescription(value: ownerId))\n"
        _result += " ownerUser: \(unwrapedDescription(value: ownerUsername))\n"
        _result += " ownerName: \(unwrapedDescription(value: ownerName))\n"
        _result += " ownerURL: \(unwrapedDescription(value: ownerURL))\n"
        return _result
    }
    
    func mapping(map: Map) {
        ownerId             <- map["id"]
        ownerURL            <- map["url"]
        ownerUsername       <- map["login"]
        ownerName           <- map["name"]
        ownerAvatar         <- map["avatar_url"]
    }
    
}

