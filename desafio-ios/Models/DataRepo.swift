//
//  DataRepo.swift
//  desafio-ios
//
//  Created by Alessandra Pereira on 31/12/2017.
//  Copyright © 2018 Alessandra Pereira. All rights reserved.
//

import Foundation
import ObjectMapper

class DataRepo: Mappable{

    var repoId: Int?
    var repoURL: String?
    var repoName: String?
    var repoFullName: String?
    var repoDesc: String?
    var forksCount: Int?
    var stargazersCount: Int?
    var openIssues: Int?
    var ownerId: Int?
    var ownerURL: String?
    var ownerUsername: String?
    var ownerAvatar: String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    private func unwrapedDescription(value: Any?) -> String {
        if let value = value {
            return "\(value)"
        }
        return "[no data]"
    }
    
    var description: String {
        var _result = " repoId: \(unwrapedDescription(value: repoId))\n"
        _result += " repoName: \(unwrapedDescription(value: repoName))\n"
        _result += " ownerId: \(unwrapedDescription(value: ownerId))\n"
        _result += " ownerURL: \(unwrapedDescription(value: ownerURL))\n"
        _result += " forks: \(unwrapedDescription(value: forksCount))\n"
        _result += " stars: \(unwrapedDescription(value: stargazersCount))\n"
        return _result
    }
    
    func mapping(map: Map) {
        repoId              <- map["id"]
        repoURL             <- map["url"]
        repoName            <- map["name"]
        repoFullName        <- map["full_name"]
        repoDesc            <- map["description"]
        forksCount          <- map["forks_count"]
        stargazersCount     <- map["stargazers_count"]
        openIssues          <- map["open_issues"]
        ownerId             <- map["owner.id"]
        ownerURL            <- map["owner.url"]
        ownerUsername       <- map["owner.login"]
        ownerAvatar         <- map["owner.avatar_url"]
    }
    
    
    
    // MARK: Endpoints
    class func endpointForRepo() -> String {
        return "https://api.github.com/search/repositories?q=language:Java&sort=stars"
    }
    
}
