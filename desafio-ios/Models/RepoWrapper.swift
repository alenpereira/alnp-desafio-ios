//
//  RepoWrapper.swift
//  desafio-ios
//
//  Created by Alessandra Pereira on 03/01/2018.
//  Copyright © 2018 Alessandra Pereira. All rights reserved.
//

import Foundation
import Alamofire

class RepoWrapper{
    
    // Can't init is singleton
    private init() { }
    
    // MARK: Shared Instance
    static let data = RepoWrapper()
    
    var repos: [DataRepo] = []
    var owners: [DataOwner] = []
    var total_count: Int?
    var current_page: Int?
    var page: Int = 1
    var selectedRepo: DataRepo?
    var selectedOwner: DataOwner?
    var pulls: [DataPR] = []
    var usersPulls: [DataOwner] = []
    let pagePulls: Int = 1
    var closedIssuesFromSelectedRepo: Int?
    var openIssuesFromSelectedRepo: Int?
    var URL: String?
    
    
}
